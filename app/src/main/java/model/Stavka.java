package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Stavka implements Serializable {

    @SerializedName("kolicina")
    private int kolicina;

    public Stavka(int kolicina) {
        this.kolicina = kolicina;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    @Override
    public String toString() {
        return "Stavka{" +
                "kolicina=" + kolicina +
                '}';
    }
}
