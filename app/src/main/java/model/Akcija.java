package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Akcija implements Serializable {
    @SerializedName("procenat")
    private int procenat;
    @SerializedName("odKad")
    private Date odKad;
    @SerializedName("doKad")
    private Date doKad;
    @SerializedName("tekst")
    private Date tekst;

    public Akcija(int procenat, Date odKad, Date doKad, Date tekst) {
        this.procenat = procenat;
        this.odKad = odKad;
        this.doKad = doKad;
        this.tekst = tekst;
    }

    public Date getTekst() {
        return tekst;
    }

    public void setTekst(Date tekst) {
        this.tekst = tekst;
    }

    public Date getDoKad() {
        return doKad;
    }

    public void setDoKad(Date doKad) {
        this.doKad = doKad;
    }

    public Date getOdKad() {
        return odKad;
    }

    public void setOdKad(Date odKad) {
        this.odKad = odKad;
    }

    public int getProcenat() {
        return procenat;
    }

    public void setProcenat(int procenat) {
        this.procenat = procenat;
    }

    @Override
    public String toString() {
        return "Akcija{" +
                "procenat=" + procenat +
                ", odKad=" + odKad +
                ", doKad=" + doKad +
                ", tekst=" + tekst +
                '}';
    }
}
