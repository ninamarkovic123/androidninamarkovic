package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Porudzbina implements Serializable {

    @SerializedName("satnica")
    private Date satnica;

    @SerializedName("dostavljeno")
    private boolean dostavljeno;

    @SerializedName("ocena")
    private int ocena;

    @SerializedName("komentar")
    private String komentar;

    @SerializedName("anonimanKomentar")
    private String anonimanKomentar;

    @SerializedName("arhiviranKomentar")
    private String arhivanKomentar;

    public Porudzbina(Date satnica,int ocena,String komentar,String anonimanKomentar,String arhivanKomentar) {
        this.satnica = satnica;
        this.ocena=ocena;
        this.komentar=komentar;
        this.anonimanKomentar=anonimanKomentar;
        this.arhivanKomentar=arhivanKomentar;

    }

    public Date getSatnica() {
        return satnica;
    }

    public void setSatnica(Date satnica) {
        this.satnica = satnica;
    }

    public boolean isDostavljeno() {
        return dostavljeno;
    }

    public void setDostavljeno(boolean dostavljeno) {
        this.dostavljeno = dostavljeno;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getAnonimanKomentar() {
        return anonimanKomentar;
    }

    public void setAnonimanKomentar(String anonimanKomentar) {
        this.anonimanKomentar = anonimanKomentar;
    }

    public String getArhivanKomentar() {
        return arhivanKomentar;
    }

    public void setArhivanKomentar(String arhivanKomentar) {
        this.arhivanKomentar = arhivanKomentar;
    }

    @Override
    public String toString() {
        return "Porudzbina{" +
                "satnica=" + satnica +
                ", dostavljeno=" + dostavljeno +
                ", ocena=" + ocena +
                ", komentar='" + komentar + '\'' +
                ", anonimanKomentar='" + anonimanKomentar + '\'' +
                ", arhivanKomentar='" + arhivanKomentar + '\'' +
                '}';
    }
}
