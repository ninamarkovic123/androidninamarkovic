package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Prodavac implements Serializable {

    @SerializedName("poslujeOd")
    private Date poslujeOd;

    @SerializedName("email")
    private String email;

    @SerializedName("adresa")
    private String adresa;

    @SerializedName("naziv")
    private String naziv;

    public Prodavac(Date poslujeOd,String email,String adresa,String naziv) {
        this.poslujeOd = poslujeOd;
        this.email=email;
        this.adresa=adresa;
        this.naziv=naziv;
    }

    public Date getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(Date poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public String toString() {
        return "Prodavac{" +
                "poslujeOd=" + poslujeOd +
                ", email='" + email + '\'' +
                ", adresa='" + adresa + '\'' +
                ", naziv='" + naziv + '\'' +
                '}';
    }
}
