package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Kupac implements Serializable {

    @SerializedName("adresa")
    private String adresa;

    public Kupac(String adresa) {
        this.adresa = adresa;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Kupac{" +
                "adresa='" + adresa + '\'' +
                '}';
    }
}
